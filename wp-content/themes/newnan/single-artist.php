<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Newnan
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) : the_post();
				$currentArtistID = get_the_id();
				$args = array(
					'post_type' => 'artist',
					'post__in' => array(get_the_id()),
					'posts_per_page' => 1
				);
				get_single_artist($args, false);
			endwhile; // End of the loop.
			?>
			<?php
				// Recent artists
				$featuredArtists = get_field('featured_artists', get_option('page_on_front'));
				$args = array(
					'post_type'    => 'artist',
					'posts_per_page' => 6,
					'post__in' => $featuredArtists,
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'slug',
							'terms' => 'current-artist',
							'operator' => 'NOT IN'
						)
					)
				);
				$recentArtistQuery = new WP_Query( $args );
				if ( $recentArtistQuery->have_posts() ) :
					$artistTotal = $recentArtistQuery->post_count;
					$artistCount = 1; ?>
					<section id="recent-artists-container" class="content-section content-inner">
						<h2 class="grey-header no-border">Other Recent Artists</h2>
						<div id="recent-artists">
							<?php while ( $recentArtistQuery->have_posts() ) : $recentArtistQuery->the_post();
								$artist_image = '';
								if($artist_portrait = get_the_post_thumbnail_url(get_the_id(), 'medium')) {
									$artist_image = $artist_portrait;
								}else{
									if($artist_art = get_field('featured_artwork')){
										$artist_image = $artist_art['sizes']['medium'];
									}
								}
								$artistLink = get_permalink();
								$artistTarget = '_self';
								if(get_field('link_to_website') && get_field('artist_website')) {
									$artistLink = get_field('artist_website');
									$artistTarget = '_blank';
								} ?>
								<div class="recent-artist">
									<div class="recent-artist-wrapper">
										<div class="recent-artist-image bg-centered" style="background-image:url(<?php echo $artist_image; ?>);">
											<div class="sizer-100"></div>
										</div>
										<div class="recent-artist-info">
											<div class="artist-info">
												<h4><a target="<?php echo $artistTarget; ?>" href="<?php echo $artistLink; ?>"><?php the_title(); ?></a></h4>
												<p><span><a target="<?php echo $artistTarget; ?>" href="<?php echo $artistLink; ?>"><?php echo get_field('artist_title') ? get_field('artist_title') : 'artist'; ?></a></span></p>
											</div>
										</div>
										<a class="link-cover" target="<?php echo $artistTarget; ?>" href="<?php echo $artistLink; ?>"></a>
									</div>
								</div>
								<?php if($artistTotal > 9 && $artistCount === $artistTotal) { ?>
									</div>
								<?php } ?>
							<?php
							endwhile;
							wp_reset_postdata(); ?>
						</div>
					</section>
					<?php
				endif; ?>
			<?php show_apply_violator(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
