jQuery(document).ready(function() {
  if(jQuery('body').hasClass('home')){
    jQuery(document).on('click', '.anchor-navigation', function (event) {
        event.preventDefault();
        var headerHeight = jQuery('.site-header').height();
        if(jQuery(window).width() < 600) {
          headerHeight = 0;
        }
        var slug = jQuery(this).attr('href').split('#')[1];
        if(jQuery('body').hasClass('menu-active')){
          jQuery('body').removeClass('menu-active');
        }
        jQuery('html, body').animate({
            scrollTop: jQuery('#'+slug).offset().top - headerHeight
        }, 500);
    });
  }
  jQuery('.menu-toggle').click(function(){
    jQuery('body').toggleClass('menu-active');
  });

  if(jQuery('.see-more-artists').length) {
    jQuery('.see-more-artists').click(function(e){
      jQuery('#recent-artists').toggleClass('show-all-artists');
      if(jQuery('#recent-artists').hasClass('show-all-artists')) {
        var innerHeight = jQuery('.more-artists-container').innerHeight();
        jQuery('.more-artists').css('max-height', innerHeight + 'px');
      } else {
        jQuery('.more-artists').css('max-height', 0 + 'px');
      }
      e.preventDefault();

    });
  }

  if(jQuery('#home-hero').length) {
    jQuery('#home-hero').imagesLoaded( { background: true }, function() {
      jQuery('#home-hero').addClass('loaded');
    });
  }

});
