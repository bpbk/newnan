<?php
//Template Name:  Apply Page
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section id="home-about" class="content-inner"><!-- ABOUT SECTION -->
				<div id="home-about-container" class="content-section">
					<?php
					if( $apply_copy = get_field('apply_header_copy')) { ?>
						<div class="about-copy basic-copy">
							<?php echo $apply_copy; ?>
						</div>
					<?php
				} ?>
				</div>
				<?php
				if($c_images = get_field('corner_images')) { ?>
					<div class="corner-image-boxes top-left-images">
						<?php
						foreach($c_images as $c_image) { ?>
							<div class="corner-box">
								<div class="corner-box-content">
									<div
									class="corner-image bg-centered"
									style="background-image:url(<?php echo $c_image['sizes']['medium']; ?>);">
									</div>
								</div>
							</div>
						<?php
						}
						?>
						<div class="corner-box hidden">
							<div class="corner-box-content">
							</div>
						</div>
					</div>
				<?php
				} ?>
			</section><!-- HOME SECTION -->
			<div class="content-inner">
				<?php
				while ( have_posts() ) : the_post(); ?>
          <section id="contact-form-container" class="content-section smaller-inner">
            <h2 class="grey-header">Application Form</h2>
            <?php the_content(); ?>
          </section>
				<?php
				endwhile; // End of the loop.
				?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
