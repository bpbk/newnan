<?php
//Template Name:  Contact Page
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content-inner">
				<?php
				while ( have_posts() ) : the_post(); ?>
          <header class="entry-header content-section">
            <h1 class="entry-title">
              <?php the_title(); ?>
            </h1>
          </header>
          <div id="contact-info-container" class="content-section smaller-inner">
            <div id="contact-map-container">
              <div id="contact-map">
                <div id="map-image" class="bg-centered" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/map.png);">
                </div>
              </div>
            </div>
            <div id="contact-info">
              <div id="info-contact" class="c-info">
                <h2 class="grey-font">Contact</h2>
                <p><a href="mailto:newnanartrez@gmail.com">newnanartrez@gmail.com</a></p>
                <a class="nar-underline black" href="mailto:newnanartrez@gmail.com">email us</a>
              </div>
							<?php if($calfile = get_field('calendar_file')) { ?>
								<div id="info-calendar" class="c-info">
	                <h2 class="grey-font">Calendar</h2>
	                <?php
									if($calLabel = get_field('calendar_label')) { ?>
										<p><?php echo $calLabel; ?></p>
									<?php
									} ?>
									<a target="_blank" class="nar-underline black" href="<?php echo $calfile; ?>">
										download
									</a>
	              </div>
							<?php } ?>
            </div>
          </div>
          <section id="contact-form-container" class="content-section smaller-inner">
            <h2 class="grey-header">Contact Form</h2>
            <?php echo do_shortcode('[caldera_form id="CF5af302f93d9d6"]'); ?>
          </section>
				<?php
				endwhile; // End of the loop.
				?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
