<?php
function show_faqs( $atts ) {
  $a = shortcode_atts( array(), $atts );
  echo get_faqs();
}
add_shortcode( 'faqs', 'show_faqs' );

function get_faqs() {
  ob_start();
  if($faqs = get_field('faqs', 'option')){ ?>
    <section class="faq-section content-section">
      <h1>Frequently Asked Questions</h1>
      <div class="faq-container">
        <?php
        $faqCount = count($faqs);
        foreach($faqs as $key=>$faq) {
          if($key === 3 && $faqCount > 4) { ?>
            <div class="more-faqs">
              <div class="more-faqs-container">
          <?php } ?>
          <div class="faq-row">
            <div class="faq-question faq-item basic-copy">
              <?php print $faq['question']; ?>
            </div>
            <div class="faq-answer faq-item basic-copy">
              <?php print $faq['answer']; ?>
            </div>
          </div>
          <?php
          if($key === $faqCount - 1 && $key > 5){ ?>
              </div>
            </div>
          <?php
          }
        } ?>
        <a href="#" class="grey-text nar-underline see-more-faqs" style="font-size: 1.2em;"></a>
      </div>
    </section>
    <script>
      if(jQuery('.more-faqs').length) {
        jQuery('.see-more-faqs').click(function(e){
          jQuery('.faq-section').toggleClass('show-all-faqs');
          if(jQuery('.faq-section').hasClass('show-all-faqs')){
            var innerHeight = jQuery('.more-faqs-container').innerHeight();
            jQuery('.more-faqs').css('max-height', innerHeight + 'px');
          } else {
            jQuery('.more-faqs').css('max-height', 0 + 'px');
          }
          e.preventDefault();

        })
      }
    </script>
  <?php
  }

  return ob_get_clean();
}

function underline_link( $atts ) {
  $a = shortcode_atts( array(
    'link' => '',
    'text' => ''
  ), $atts );
  return "<a class='grey-text nar-underline' style='font-size: 1.2em;' target='_blank' href='".$a['link']."'>".$a['text']."</a>";
}
add_shortcode( 'underlinelink', 'underline_link' );
