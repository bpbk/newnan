<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Newnan
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function newnan_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'newnan_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function newnan_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'newnan_pingback_header' );

/*--------------------------------*\
	CUSTOM POST TYPES
\*--------------------------------*/

function register_custom_post_types() {

	$labels = array(
    'name' => _x('Artists', 'post type general name'),
    'singular_name' => _x('Artist', 'post type singular name'),
    'add_new' => _x('Add New', 'artist'),
    'add_new_item' => __('Add New Artist'),
    'edit_item' => __('Edit Artist'),
    'new_item' => __('New Artist'),
    'view_item' => __('View Artist'),
    'search_items' => __('Search Artists'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
	);

	$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies' => array('category'),
  );
	register_post_type( 'artist' , $args );
}

add_action('init', 'register_custom_post_types');

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'Newnan Options',
		'menu_slug' 	=> 'newnan-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'FAQ',
		'menu_title'	=> 'FAQ',
		'parent_slug'	=> 'newnan-options',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Apply Now Violator',
		'menu_title'	=> 'Apply Now Violator Fields',
		'parent_slug'	=> 'newnan-options',
	));

}

function show_apply_violator() {
	ob_start();
	$applyLink = get_permalink(67);
	$applyBg = get_field('apply_background_image', 'option')['sizes']['medium'];
	$applyCopy = get_field('apply_left_side_copy', 'option'); ?>
	<div class="apply-violator bg-centered" style="background-image:url(<?php echo $applyBg; ?>);">
		<div class="dark-overlay"></div>
		<div class="apply-violator-content">
			<div class="apply-violator-copy">
				<?php echo $applyCopy; ?>
			</div>
			<a class="apply-link" href="<?php echo $applyLink; ?>">Apply Now</a>
		</div>
	</div>
	<?php
	echo ob_get_clean();
}

function get_single_artist($args = false, $title = true) {
		$currentArtistQuery = new WP_Query( $args );

		if ( $currentArtistQuery->have_posts() ) : ?>
			<section id="artists-block" class="content-section content-inner">
				<div class="current-artist">
					<?php while ( $currentArtistQuery->have_posts() ) : $currentArtistQuery->the_post();
						if($title){ ?>
							<h2 class="grey-header no-border">Current Artist</span></h2>
						<?php
						} ?>
						<div class="artist-content">
							<?php
							$artist_image = '';
							if($artist_thumb = get_the_post_thumbnail_url(get_the_id(), 'medium')){
								$artist_image = $artist_thumb;
							} else {
								if($artist_art = get_field('featured_artwork')){
									$artist_image = $artist_art['sizes']['medium'];
								}
							} ?>

							<div class="artist-image-container">
								<div class="sizer-100"></div>
								<div class="artist-image bg-centered" style="background-image:url(<?php echo $artist_image; ?>);">
								</div>
							</div>

							<div class="artist-description basic-copy">
								<h1><?php the_title(); ?></h1>
								<?php
								if($resDate = get_field('residency_date')){ ?>
									<span class="nar-date-grey"><?php echo $resDate; ?></span>
								<?php
								}
								if($title) {
									if(has_excerpt()) {
										the_excerpt();
									}	else { ?>
										<p>
											<?php echo excerpt(80); ?>
										</p>
									<?php
									}
								} else {
									the_content();
								}

								if ($artist_site = get_field('artist_website')) { ?>
									<a
										target="_blank"
										class="nar-underline black"
										href="<?php echo $artist_site; ?>">website</a>
								<?php
								}
								if($title) { ?>

									<div style="margin-top: 25px;">See <a style="font-weight: bold;" href="https://www.facebook.com/newnanartrez/" target="_blank">Facebook</a> for scheduled events.</div>
								<?php } ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
			<?php
			wp_reset_postdata();
		endif;
}

function show_more_artists($args, $images = true, $count = 1) {
	$recentArtistQuery = new WP_Query( $args );
	if ( $recentArtistQuery->have_posts() ) :
		$artistTotal = $recentArtistQuery->post_count;
		$artistCount = $count;
		$currentCount = 1;
		while ( $recentArtistQuery->have_posts() ) : $recentArtistQuery->the_post();
			$artistTarget = '_self';
			$artistLink = '';
			if($images) {
				$artistLink = get_permalink();
			}
			if(get_field('artist_website')) {
				if($images && get_field('link_to_website') || !$images) {
					$artistLink = get_field('artist_website');
					$artistTarget = '_blank';
				}
			}
			$artist_image = '';
			if($images) {
				if($artist_art = get_field('featured_artwork')){
					$artist_image = $artist_art['sizes']['medium'];
				} else {
					$artist_image = get_the_post_thumbnail_url(get_the_id(), 'medium');
				}
			}
			if($artistCount === 25) { ?>
				<div class="more-artists">
					<div class="more-artists-container">
			<?php
			}
			?>
			<div class="recent-artist<?php echo $images ? '' : ' no-images'; ?>">
				<div class="recent-artist-wrapper">
					<?php if($images) { ?>
						<div class="recent-artist-image bg-centered" style="background-image:url(<?php echo $artist_image; ?>);">
							<div class="sizer-100"></div>
						</div>
					<?php } ?>
					<div class="recent-artist-info">
						<div class="artist-info">
							<h4>
								<?php if(isset($artistLink) && $artistLink != '') { ?>
									<a target="<?php echo $artistTarget; ?>" href="<?php echo $artistLink; ?>">
								<?php } ?>
										<?php the_title(); ?>
								<?php if(isset($artistLink) && $artistLink != '') { ?>
									</a>
								<?php } ?>
							</h4>
							<p><span>
								<?php if(isset($artistLink) && $artistLink != '') { ?>
									<a target="<?php echo $artistTarget; ?>" href="<?php echo $artistLink; ?>">
								<?php } ?>
									<?php echo get_field('artist_title') ? get_field('artist_title') : 'artist'; ?>
								<?php if(isset($artistLink) && $artistLink != '') { ?>
									</a>
								<?php } ?>
							</span></p>
							<?php
							if($resDate = get_field('residency_date')){
								if(!$images) { ?>
									<span style="color: rgb(180,180,180); font-style: italic; font-size: .65em; letter-spacing: 1px;"><?php echo $resDate; ?></span>
								<?php
								}
							} ?>
						</div>
					</div>
					<?php if($images) { ?>
						<a target="<?php echo $artistTarget; ?>" class="link-cover" href="<?php echo $artistLink; ?>"></a>
					<?php } ?>
				</div>
			</div>
			<?php
			if($artistTotal + $count > 25 && $currentCount === $artistTotal) { ?>
					</div>
				</div>
			<?php } ?>
		<?php
		$artistCount++;
		$currentCount++;
		endwhile;
		wp_reset_postdata(); ?>
	<?php
	endif;
	if($artistCount > 25) { ?>
		<a href="#" class="see-more-artists nar-underline black"></a>
	<?php
	}
}

function display_social_icons() { ?>
	<div id="social-icons">
		<a id="social-icon-fb" class="social-icon" href="https://facebook.com/newnanartrez" target="_blank">
			<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="430.113px" height="430.114px" viewBox="0 0 430.113 430.114" style="enable-background:new 0 0 430.113 430.114;"
				 xml:space="preserve">
				<g>
					<path id="Facebook" d="M158.081,83.3c0,10.839,0,59.218,0,59.218h-43.385v72.412h43.385v215.183h89.122V214.936h59.805
						c0,0,5.601-34.721,8.316-72.685c-7.784,0-67.784,0-67.784,0s0-42.127,0-49.511c0-7.4,9.717-17.354,19.321-17.354
						c9.586,0,29.818,0,48.557,0c0-9.859,0-43.924,0-75.385c-25.016,0-53.476,0-66.021,0C155.878-0.004,158.081,72.48,158.081,83.3z"/>
				</g>
			</svg>
		</a>
		<a id="social-icon-insta" class="social-icon" href="https://instagram.com/newnanartrez" target="_blank">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="169.063px" height="169.063px" viewBox="0 0 169.063 169.063" style="enable-background:new 0 0 169.063 169.063;"
				 xml:space="preserve">
				<g>
					<path d="M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752
						c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407
						c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752
						c17.455,0,31.656,14.201,31.656,31.655V122.407z"/>
					<path d="M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561
						C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561
						c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z"/>
					<path d="M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78
						c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78
						C135.661,29.421,132.821,28.251,129.921,28.251z"/>
				</g>
			</svg>
		</a>
		<a id="social-icon-li" class="social-icon" href="https://www.linkedin.com/company/newnanartrez/" target="_blank">
			<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="430.117px" height="430.117px" viewBox="0 0 430.117 430.117" style="enable-background:new 0 0 430.117 430.117;"
				 xml:space="preserve">
				<g>
					<path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707
						c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21
						v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824
						C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463
						c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z
						 M5.477,420.56h92.184v-277.32H5.477V420.56z"/>
				</g>
			</svg>
		</a>
	</div>
<?php
}

function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
