<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newnan
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i,700" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'newnan' ); ?></a>
	<div class="header-placeholder">
	</div>
	<?php if ( !is_front_page() && !is_home() ) : ?>
		<div class="header-brush-container">
			<div class="content-inner">
				<div class="header-brush">
					<img src="<?php echo get_template_directory_uri().'/images/brush_3.png'; ?>"/>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<header id="masthead" class="site-header">
		<div class="content-inner">
			<div id="header-content">
				<div class="site-branding">
					<!-- <?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) :
						?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php
					else :
						?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
					endif; ?> -->
				</div><!-- .site-branding -->
				<div class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<div class="menu-toggle-item"></div>
				</div>
				<div id="navigation-container">
					<div id="navigation-content">
						<nav id="site-navigation" class="main-navigation">
							<div class="nav-brush">
								<img src="<?php echo get_template_directory_uri().'/images/brush_2.png'; ?>"/>
							</div>
							<div>
								<ul>
									<?php
									$navItems = wp_get_nav_menu_items( 'menu-1' );
									foreach($navItems as $navItem) { ?>
										<li>
											<a class="anchor-navigation" href="<?php echo home_url().$navItem->url; ?>">
												<?php echo $navItem->title; ?>
											</a>
										</li>
									<?php
								} ?>
							</ul>
								<?php
								// wp_nav_menu( array(
								// 	'theme_location' => 'menu-1',
								// 	'menu_id'        => 'primary-menu',
								// ) );
								wp_nav_menu( array(
									'menu' => 'Second Menu'
								) )
								?>
							</div>
							<?php display_social_icons(); ?>
						</nav><!-- #site-navigation -->
						<?php show_apply_violator(); ?>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
