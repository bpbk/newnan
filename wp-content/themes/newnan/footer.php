<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newnan
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="content-inner">
			<div class="footer-content">
				<div class="footer-logo">
					<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"/>
				</div>
				<div class="site-info">
					<p><a href="mailto:newnanartrez@gmail.com">newnanartrez@gmail.com</a><p>
				</div><!-- .site-info -->
				<div class="footer-apply">
					<p><span><a href="<?php echo get_permalink(67); ?>">Apply</a></span> to become an artist in residence</p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
