<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Newnan
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<!-- HOME SECTION -->
			<?php
			$hero_bg = get_field('hero_background_image'); ?>
			<section id="home-hero" class="content-inner">
				<div id="home-hero-overlay"></div>
				<div id="home-hero-container" class="bg-centered" style="background-image:url(<?php echo $hero_bg['sizes']['large']; ?>)">
					<div class="hero-copy-container">
						<img id="hero-logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"/>
						<?php
						if( $hero_copy = get_field('hero_copy')) { ?>
							<div class="hero-copy">
								<h1><?php echo $hero_copy; ?></h1>
							</div>
						<?php
						} ?>
					</div>
				</div>
				<?php
				if($c_images = get_field('hero_corner_images')) { ?>
					<div class="corner-image-boxes bottom-right-images">
						<div class="corner-box hidden">
							<div class="corner-box-content">
							</div>
						</div>
						<?php
						foreach($c_images as $c_image) { ?>
							<div class="corner-box">
								<div class="corner-box-content">
									<div
									class="corner-image bg-centered"
									style="background-image:url(<?php echo $c_image['sizes']['medium']; ?>);">
									</div>
								</div>
							</div>
						<?php
						}
						?>
					</div>
				<?php
				} ?>
			</section><!-- HOME SECTION -->

			<a class="anchor-tag" id="about" name="about"></a>
			<section id="home-about" class="content-inner"><!-- ABOUT SECTION -->
				<div id="home-about-container" class="content-section">
					<?php
					if( $about_copy = get_field('about_copy')) { ?>
						<div class="about-copy basic-copy">
							<?php echo $about_copy; ?>
						</div>
					<?php
				} ?>
				</div>
				<?php
				if($c_images = get_field('about_corner_images')) { ?>
					<div class="corner-image-boxes top-left-images">
						<?php
						foreach($c_images as $c_image) { ?>
							<div class="corner-box">
								<div class="corner-box-content">
									<div
									class="corner-image bg-centered"
									style="background-image:url(<?php echo $c_image['sizes']['medium']; ?>);">
									</div>
								</div>
							</div>
						<?php
						}
						?>
						<div class="corner-box hidden">
							<div class="corner-box-content">
							</div>
						</div>
					</div>
				<?php
				} ?>
			</section><!-- HOME SECTION -->

			<!-- FAQ SECTION -->
			<a class="anchor-tag" id="faq" name="faq"></a>
			<div class="content-inner">
				<?php echo get_faqs(); ?>
			</div>
			<!-- FAQ SECTION -->

			<!-- Apply Violator -->
			<?php echo show_apply_violator(); ?>
			<!-- Apply Violator -->

			<!-- ARTISTS SECTION -->
			<a class="anchor-tag" id="artists" name="artists"></a>

			<?php
			$args = array(
				'post_type'    => 'artist',
				'category_name' => 'current-artist',
				'posts_per_page' => 1,
			);
			get_single_artist($args, true);
			$featuredArtists = get_field('featured_artists', get_option('page_on_front'));
      $args = array(
        'post_type'    => 'artist',
        'posts_per_page' => -1,
				'post__in' => $featuredArtists,
				'orderby' => 'post__in',
				'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'field' => 'slug',
						'terms' => 'current-artist',
						'operator' => 'NOT IN'
					)
				)
      ); ?>
			<section id="recent-artists-container" class="content-section content-inner">
				<h2 style="padding-top: 1em; margin-bottom: 0;" class="grey-header no-border"><?php echo 'Past Artists'; ?></h2>
				<div id="recent-artists" style="margin-top: 1em;">
					<?php show_more_artists($args, true); ?>
				</div>
			</section>
			<?php
			// show all the rest.
			$args = array(
	      'post_type'    => 'artist',
	      'posts_per_page' => -1,
				'post__not_in' => $featuredArtists,
				'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'field' => 'slug',
						'terms' => 'current-artist',
						'operator' => 'NOT IN'
					)
				)
	    ); ?>
			<section id="recent-artists-list" class="content-section content-inner">
				<?php show_more_artists($args, false); ?>
			</section>
	<!-- ARTISTS SECTION -->

			<section id="gray-cottage" class="content-section content-inner"><!--- GRAY COTTAGE SECTION -->
				<?php
				if($gc_images = get_field('gray_cottage_images')) { ?>
					<div class="image-gallery-container">
						<div class="image-gallery">
							<?php
							foreach($gc_images as $gc_image) { ?>
								<div class="gallery-image-wrapper">
									<div class="gallery-image-container">
										<div
										class="gallery-image bg-centered"
										style="background-image:url(<?php echo $gc_image['sizes']['medium']; ?>);">
										</div>
									</div>
								</div>
							<?php
							}
							?>
						</div>
						<div class="gallery-arrows">
							<div class="gallery-arrow-left">
								<i></i>
							</div>
							<div class="gallery-arrow-right">
								<i></i>
							</div>
						</div>
					</div>
				<?php
				} ?>
				<div id="gray-cottage-container" class="content-section">
					<?php
					if( $gray_cottage_copy = get_field('gray_cottage_copy')) { ?>
						<div class="cottage-copy basic-copy">
							<?php echo $gray_cottage_copy; ?>
						</div>
					<?php
				} ?>
				</div>
				<script>
					jQuery('.image-gallery').slick({
						'dots': false,
						'arrows': true,
						'fade' : true,
						'prevArrow' : jQuery('.gallery-arrow-left'),
						'nextArrow' : jQuery('.gallery-arrow-right'),
					});
				</script>
			</section><!-- GRAY COTTAGE SECTION -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
